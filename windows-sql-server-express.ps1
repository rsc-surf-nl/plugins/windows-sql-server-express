$LOGFILE = "c:\logs\windows-sql-server-express.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

function download_extract ($name, $url, $extractpath, $hash, $unzip) {
  $fallback = $false
  if (!(Test-Path $extractpath)) {
      Write-Log "Create extact Folder $extractpath"
      New-Item -Path $extractpath -ItemType Directory -Force
  }

  Import-Module BitsTransfer
  Start-BitsTransfer -Source $url -Destination $extractpath -Asynchronous -DisplayName $name -TransferType Download

  while ((Get-BitsTransfer -Name $name).jobstate -eq "Connecting") {
      Start-Sleep -Seconds 1
      Write-Log "Connecting $url to download"
  }

  if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") {
      Write-Log "downloading $name"
      while ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") { 
          Start-Sleep -Seconds 3 
      }
  
      if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferred") {
          Write-Log "downloading $name is completed"
          Get-BitsTransfer -Name $name | Complete-BitsTransfer
      } else {
          $fallback = $true
      }
  } else {
      Write-Log "unable to download with BitsTransfer try with webrequest"
      $fallback = $true
  }

  if ($fallback) {
      #fallback download
      Invoke-WebRequest -Uri $url -OutFile $extractpath
  }

  #get filename
  $downloadfilename = Get-ChildItem $extractpath
  Write-Log "Downloaded file is $downloadfilename"

  if ($downloadfilename) {
      Write-Log "Download complete"
      
      #check if hash equals file
      # Calculate the SHA-256 hash of the file
      $calculatedHash = Get-FileHash -Path $downloadfilename.PSPath -Algorithm SHA256 | Select-Object -ExpandProperty Hash

      # Compare the calculated hash with the expected hash
      if ($calculatedHash -eq $hash) {
          Write-Log "The $name hash matches the expected hash."
          $result = 1
      } else {
          Write-Log "The $name hash ($calculatedHash) does not match the expected hash ($hash)."
          return 0
      }
  } else {
      Write-Log "Unable to download $url"
      $result = 0
  }

  if ($unzip) {
      Expand-Archive -Path $downloadfilename.PSPath -DestinationPath $extractpath
      $result = 1
  }

  return $result
}

Function Main {
 
  try {
      Write-Log "Start windows-sql-server-express"
      $downloadresult = download_extract -name "sql-server-express" -url "https://download.microsoft.com/download/3/8/d/38de7036-2433-4207-8eae-06e247e17b25/SQLEXPR_x64_ENU.exe" -extractpath "$env:temp\sql-server-express" -hash "2E61C8BBDE6021F9026C54AD9DB4BBB1227E68761D4C00A6A50A2C70FE7AFE05" -unzip $false
      if ($downloadresult -like "1") {
        Write-Log "install sql-server-express"
        Start-Process -FilePath "$env:temp\sql-server-express\SQLEXPR_x64_ENU.exe" -ArgumentList "/IACCEPTSQLSERVERLICENSETERMS /Q /ACTION=install /INSTANCEID=SQLEXPRESS /INSTANCENAME=SQLEXPRESS /UPDATEENABLED=FALSE" -NoNewWindow -Wait
      } else {
        Write-Log "Unable to install the download check the log"
      }
      Write-Log "Start SQL_Server_Management_Studio"
      $downloadresult2 = download_extract -name "SQL_Server_Management_Studio" -url "https://download.microsoft.com/download/9/b/e/9bee9f00-2ee2-429a-9462-c9bc1ce14c28/SSMS-Setup-ENU.exe" -extractpath "$env:temp\SSMS" -hash "12DD3B24330AD074DBB71F28AD06EAAA9757BD5C9A750ADD5088C4DB54A7D4F6" -unzip $false
      if ($downloadresult2 -like "1") {
        Write-Log "install SQL_Server_Management_Studio"
        Start-Process -FilePath "$env:temp\SSMS\SSMS-Setup-ENU.exe" -ArgumentList "/quiet /install /norestart" -NoNewWindow -Wait
      } else {
        Write-Log "Unable to install the download check the log"
      }

  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End windows-sql-server-express"
 
}

Main    
